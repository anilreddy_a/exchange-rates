package com.hsbc.exchangerates.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hsbc.exchangerates.dao.ExchangeRateDAO;
import com.hsbc.exchangerates.dto.ExchangeRateDTO;
import com.hsbc.exchangerates.dto.ExchangeRateDataDTO;
import com.hsbc.exchangerates.rest.ExchangeRateRestService;

@Service
public class ExchangeRateServiceImpl {
	
	private static final Logger logger = LoggerFactory.getLogger(ExchangeRateServiceImpl.class);
	
	@Autowired
	private ExchangeRateRestService exchangeRateRestService;
	
	@Autowired
	private ExchangeRateDAO exchangeRateDAO;

	public void loadData() {
		logger.info("ExchangeRateServiceImpl::loadData start");
		ExchangeRateDTO exchangeRateDTO = exchangeRateRestService.getExchangeRates();
		exchangeRateDAO.saveExchangeRates(exchangeRateDTO);
		logger.info("ExchangeRateServiceImpl::loadData end");
	}

	public List<ExchangeRateDataDTO> getData(String todaysDate) {
		logger.info("ExchangeRateServiceImpl::getData start");
		return exchangeRateDAO.getRates(todaysDate);
	}

}
