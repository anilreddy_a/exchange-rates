package com.hsbc.exchangerates.rest;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.hsbc.exchangerates.dto.ExchangeRateDTO;
import com.hsbc.exchangerates.service.impl.ExchangeRateServiceImpl;

@Service
public class ExchangeRateRestService {
	
	private static final Logger logger = LoggerFactory.getLogger(ExchangeRateServiceImpl.class);
	
	public static final String exchangeRatesUrl = "http://api.exchangeratesapi.io/v1/latest?access_key=fa2742d26bd42fcd61d8a209889f3d86&base=EUR&symbols=GBP,USD,HKD";

	public ExchangeRateDTO getExchangeRates() {
		logger.info("getExchangeRates start");
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<Map<String,String>> request = new HttpEntity<>(headers);
		ResponseEntity<ExchangeRateDTO> response = restTemplate
		  .exchange(exchangeRatesUrl, HttpMethod.GET, request, ExchangeRateDTO.class);
		logger.info("getExchangeRates end::"+response.getBody().getBase()); 
		return response.getBody();
	}

}
