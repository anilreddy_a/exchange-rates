package com.hsbc.exchangerates.result;

import java.io.Serializable;

public class ExchangeRatesResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7751995890666621294L;
	
	private String success;
	
	private Long timestamp;
	
	private String message;
	
	private Object resObject;
	
	private Long responseTime;
	
	public ExchangeRatesResponse(String success, Long timestamp, String message, Object resObject, Long responseTime) {
		this.success = success;
		this.timestamp = timestamp;
		this.message = message;
		this.resObject = resObject;
		this.responseTime = responseTime;
	}

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getResObject() {
		return resObject;
	}

	public void setResObject(Object resObject) {
		this.resObject = resObject;
	}

	public Long getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(Long responseTime) {
		this.responseTime = responseTime;
	}

}
