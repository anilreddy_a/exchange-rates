package com.hsbc.exchangerates.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

public class ExchangeRateDataDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4248566234622099252L;
	
	private String base;
	
	private String currency;
	
	private BigDecimal rate;
	
	private String date;
	

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}
	
}
