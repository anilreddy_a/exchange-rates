package com.hsbc.exchangerates.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

public class ExchangeRateDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2564547222599655488L;
	
	private Boolean success;
	
	private Long timestamp;
	
	private Boolean historical;
	
	private String base;
	
	private String date;
	
	private Map<String, BigDecimal> rates;

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public Boolean getHistorical() {
		return historical;
	}

	public void setHistorical(Boolean historical) {
		this.historical = historical;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Map<String, BigDecimal> getRates() {
		return rates;
	}

	public void setRates(Map<String, BigDecimal> rates) {
		this.rates = rates;
	}
	
}
