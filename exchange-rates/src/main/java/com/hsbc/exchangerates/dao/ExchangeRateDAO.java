package com.hsbc.exchangerates.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hsbc.exchangerates.dao.entity.ExchangeRateEntity;
import com.hsbc.exchangerates.dao.repository.ExchangeRateRepository;
import com.hsbc.exchangerates.dto.ExchangeRateDTO;
import com.hsbc.exchangerates.dto.ExchangeRateDataDTO;

@Component
public class ExchangeRateDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(ExchangeRateDAO.class);
	
	@Autowired
	private ExchangeRateRepository exchangeRateRepository;

	public void saveExchangeRates(ExchangeRateDTO exchangeRateDTO) {
		logger.info("ExchangeRateDAO::saveExchangeRates start");
		//Deleting existing records to keep only latest rates (Other Exchange rate APIs are not supported for free account)
		exchangeRateRepository.deleteAll();
		List<ExchangeRateEntity> exchangeRateEntities = new ArrayList<>();
		for(Map.Entry<String,BigDecimal> rates : exchangeRateDTO.getRates().entrySet()) {
			ExchangeRateEntity exchangeRateEntity = new ExchangeRateEntity();
			exchangeRateEntity.setBase(exchangeRateDTO.getBase());
			exchangeRateEntity.setCurrency(rates.getKey());
			exchangeRateEntity.setRate(rates.getValue());
			exchangeRateEntity.setDate(exchangeRateDTO.getDate());
			exchangeRateEntities.add(exchangeRateEntity);
		}
		exchangeRateRepository.saveAll(exchangeRateEntities);
		logger.info("ExchangeRateDAO::saveExchangeRates end");
	}

	public List<ExchangeRateDataDTO> getRates(String todaysDate) {
		logger.info("ExchangeRateDAO::getRates start");
		List<ExchangeRateEntity> exchangeRateEntities = exchangeRateRepository.getRecordsByTodaysDate(todaysDate);
		logger.info("ExchangeRateDAO::getRates end");
		return getExchangeRateDataDTOs(exchangeRateEntities);
	}

	private List<ExchangeRateDataDTO> getExchangeRateDataDTOs(List<ExchangeRateEntity> exchangeRateEntities) {
		
		List<ExchangeRateDataDTO> exchangeRateDataDTOs = new ArrayList<>();
		exchangeRateEntities.forEach(exchangeRateEntity->{
			ExchangeRateDataDTO exchangeRateDataDTO = new ExchangeRateDataDTO();
			exchangeRateDataDTO.setBase(exchangeRateEntity.getBase());
			exchangeRateDataDTO.setCurrency(exchangeRateEntity.getCurrency());
			exchangeRateDataDTO.setRate(exchangeRateEntity.getRate());
			exchangeRateDataDTO.setDate(exchangeRateEntity.getDate());
			exchangeRateDataDTOs.add(exchangeRateDataDTO);
		});
		
		return exchangeRateDataDTOs;
	}

}
