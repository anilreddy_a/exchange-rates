package com.hsbc.exchangerates.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hsbc.exchangerates.dao.constants.QueryConstants;
import com.hsbc.exchangerates.dao.entity.ExchangeRateEntity;

@Repository
@Transactional
public interface ExchangeRateRepository extends JpaRepository<ExchangeRateEntity, Long>{

	@Query(value=QueryConstants.GET_RATES, nativeQuery = true)
	List<ExchangeRateEntity> getRecordsByTodaysDate(@Param("date") String todaysDate);

}
