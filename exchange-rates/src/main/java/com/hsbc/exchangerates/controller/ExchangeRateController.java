package com.hsbc.exchangerates.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hsbc.exchangerates.constants.APIConstants;
import com.hsbc.exchangerates.constants.MessageConstants;
import com.hsbc.exchangerates.dto.ExchangeRateDataDTO;
import com.hsbc.exchangerates.result.ExchangeRatesResponse;
import com.hsbc.exchangerates.service.impl.ExchangeRateServiceImpl;

@RestController
@RequestMapping(APIConstants.V1)
public class ExchangeRateController {
	
	private static final Logger logger = LoggerFactory.getLogger(ExchangeRateController.class);
	
	@Autowired
	private ExchangeRateServiceImpl exchangeRateServiceImpl;
	
	@GetMapping(APIConstants.TEST)
	public String testExchangeRate() {
		return "from ExchangeRateController";
	}
	
	@PostMapping(APIConstants.RATES)
	public ResponseEntity<ExchangeRatesResponse> loadData() {
		logger.info("ExchangeRateController::loadData start");
		Long startTime = System.currentTimeMillis();
		exchangeRateServiceImpl.loadData();
		ResponseEntity<ExchangeRatesResponse> response= formResposeObject(MessageConstants.SUCCESS,startTime,null,MessageConstants.DATA_SAVED);
		logger.info("ExchangeRateController::loadData end");
		return response;
	}

	private ResponseEntity<ExchangeRatesResponse> formResposeObject(String success, Long startTime, Object resObj, String message) {
		ExchangeRatesResponse exchangeRatesResponse =  new ExchangeRatesResponse(success, System.currentTimeMillis(), message, resObj, System.currentTimeMillis()-startTime);
		return ResponseEntity.accepted().body(exchangeRatesResponse);
	}

	@GetMapping(APIConstants.RATES)
	public ResponseEntity<ExchangeRatesResponse> getData(@RequestParam String todaysDate) {
		logger.info("ExchangeRateController::getData start");
		Long startTime = System.currentTimeMillis();
		List<ExchangeRateDataDTO> exchangeRateDataDTOs = exchangeRateServiceImpl.getData(todaysDate);
		ResponseEntity<ExchangeRatesResponse> response= formResposeObject(MessageConstants.SUCCESS,startTime,exchangeRateDataDTOs,MessageConstants.DATA_RETREIVED);
		logger.info("ExchangeRateController::getData end");
		return response;
	}
}
