package com.hsbc.exchangerates.constants;

public class APIConstants {
	
	public static final String V1 = "/v1";
	public static final String TEST = "/test";
	public static final String RATES = "/rates";

}
