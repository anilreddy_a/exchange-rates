package com.hsbc.exchangerates.constants;

public class MessageConstants {
	
	public static final String SUCCESS = "success";
	
	public static final String DATA_SAVED = "Data saved successfully";
	
	public static final String DATA_RETREIVED = "Data retreived successfully";

}
